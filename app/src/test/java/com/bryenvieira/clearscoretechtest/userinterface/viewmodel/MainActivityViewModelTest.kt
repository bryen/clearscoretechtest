package com.bryenvieira.clearscoretechtest.userinterface.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.bryenvieira.clearscoretechtest.repository.CreditScoreRepository
import com.bryenvieira.clearscoretechtest.repository.model.CreditReportInfo
import com.bryenvieira.clearscoretechtest.repository.model.CreditScore
import com.bryenvieira.clearscoretechtest.userinterface.homescreen.viewmodel.MainActivityViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.timeout
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class MainActivityViewModelTest {
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var repository: CreditScoreRepository
    private lateinit var observer: Observer<CreditScore>
    private val validResponse = CreditScore(CreditReportInfo(514, 700, 0))
    private val invalidResponse = CreditScore(CreditReportInfo(999, 1, 10))
    private val nullResponse = null

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        repository = mock()
        viewModel = MainActivityViewModel(repository)
        observer = mock()
    }

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun `when getCreditScore returns valid response, then observer is updated with success`() {
        runBlocking {
            whenever(repository.getCreditScore()).thenReturn(validResponse)
        }
        runBlocking {
            viewModel.creditScore.observeForever(observer)
            delay(50)
            verify(repository).getCreditScore()
            verify(observer, timeout(50)).onChanged(validResponse)
        }
    }

    @Test
    fun `when getCreditScore returns invalid but not null response, then observer is updated with failure`() {
        runBlocking {
            whenever(repository.getCreditScore()).thenReturn(invalidResponse)
        }
        runBlocking {
            viewModel.creditScore.observeForever(observer)
            delay(50)
            verify(repository).getCreditScore()
            verify(observer, timeout(50)).onChanged(invalidResponse)
        }
    }

    @Test
    fun `when getCreditScore returns null, then observer is updated with failure`() {
        runBlocking {
            whenever(repository.getCreditScore()).thenReturn(nullResponse)
        }
        runBlocking {
            viewModel.creditScore.observeForever(observer)
            delay(50)
            verify(repository).getCreditScore()
            verify(observer, timeout(50)).onChanged(nullResponse)
        }
    }
}