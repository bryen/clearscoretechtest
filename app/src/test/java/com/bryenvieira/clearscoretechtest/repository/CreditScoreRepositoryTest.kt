package com.bryenvieira.clearscoretechtest.repository

import com.bryenvieira.clearscoretechtest.repository.api.CreditScoreApi
import com.bryenvieira.clearscoretechtest.repository.model.CreditReportInfo
import com.bryenvieira.clearscoretechtest.repository.model.CreditScore
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CreditScoreRepositoryTest {
    private lateinit var api: CreditScoreApi
    private lateinit var repository: CreditScoreRepository
    private val validResponse = CreditScore(CreditReportInfo(514, 700, 0))

    @Before
    fun setUp() {
        api = mock()
        runBlocking {
            whenever(api.getCreditScore()).thenReturn(validResponse)
        }
        repository = CreditScoreRepository(api)
    }

    @Test
    fun `when getCreditScore is called, then a creditScore model object is returned`() =
        /* This is a pretty useless test right now because we don't do anything interesting with
         the API right now, but nonetheless, it's useful to have this boilerplate for later */
        runBlocking {
            assertEquals(validResponse, repository.getCreditScore())
        }
}