package com.bryenvieira.clearscoretechtest

import android.app.Application
import com.bryenvieira.clearscoretechtest.repository.api.networkModule
import com.bryenvieira.clearscoretechtest.repository.creditScoreModule
import com.bryenvieira.clearscoretechtest.userinterface.homescreen.viewmodel.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class ClearScoreTechTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@ClearScoreTechTestApplication)
            modules(
                listOf(
                    viewModelModule,
                    networkModule,
                    creditScoreModule
                )
            )
        }
    }
}