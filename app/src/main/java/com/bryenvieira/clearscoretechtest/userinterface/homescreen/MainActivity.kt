package com.bryenvieira.clearscoretechtest.userinterface.homescreen

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.ComponentActivity
import androidx.lifecycle.Observer
import app.futured.donut.DonutSection
import com.bryenvieira.clearscoretechtest.R
import com.bryenvieira.clearscoretechtest.databinding.MainActivityBinding
import com.bryenvieira.clearscoretechtest.repository.model.CreditScore
import com.bryenvieira.clearscoretechtest.userinterface.homescreen.viewmodel.MainActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : ComponentActivity() {

    private val viewModel: MainActivityViewModel by viewModel()
    private val observer = Observer<CreditScore> {
        updateUi(it)
    }
    private lateinit var binding: MainActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initUi()

        viewModel.creditScore.observe(this, observer)
    }

    private fun updateUi(creditScore: CreditScore?) {
        if (creditScore == null) {
            toggleErrorUi(true)
        } else {
            val uiModel = viewModel.getUiModel(creditScore)
            if (uiModel == null) {
                toggleErrorUi(true)
                return
            }
            toggleErrorUi(false)
            with(uiModel) {
                binding.donutView.gapWidthDegrees = gapWidth
                binding.donutView.masterProgress = masterProgress
                binding.creditScore.text = score
            }
        }
    }

    private fun initUi() {
        binding.donutView.submitData(
            listOf(
                DonutSection(
                    CREDIT_SCORE_SECTION_NAME,
                    getColor(R.color.orange),
                    CREDIT_SCORE_SECTION_SIZE
                )
            )
        )
    }

    private fun toggleErrorUi(showError: Boolean) {
        binding.scoreFetchError.visibility = if (showError) View.VISIBLE else View.GONE
        binding.scoreLl.visibility = if (showError) View.GONE else View.VISIBLE
        binding.donutView.bgLineColor = if (showError) Color.RED else Color.GRAY
        binding.donutView.masterProgress = 0f
    }
}

private const val CREDIT_SCORE_SECTION_NAME = "credit_score"
private const val CREDIT_SCORE_SECTION_SIZE = 1f
