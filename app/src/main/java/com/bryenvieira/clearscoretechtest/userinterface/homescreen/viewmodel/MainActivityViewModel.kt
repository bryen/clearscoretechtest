package com.bryenvieira.clearscoretechtest.userinterface.homescreen.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.bryenvieira.clearscoretechtest.repository.CreditScoreRepository
import com.bryenvieira.clearscoretechtest.repository.model.CreditScore
import com.bryenvieira.clearscoretechtest.userinterface.homescreen.CreditScoreUiModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainActivityViewModel(get()) }
}

class MainActivityViewModel(private val creditScoreRepository: CreditScoreRepository) :
    ViewModel() {

    val creditScore: LiveData<CreditScore> = liveData {
        emit(creditScoreRepository.getCreditScore())
    }

    fun getUiModel(creditScore: CreditScore): CreditScoreUiModel? {
        with(creditScore.creditReportInfo) {
            if (minScoreValue > maxScoreValue || score < minScoreValue || score > maxScoreValue) {
                return null
            }
            return CreditScoreUiModel(
                gapWidth =
                (creditScore.creditReportInfo.minScoreValue.toFloat() / creditScore.creditReportInfo.maxScoreValue * 360),
                masterProgress =
                creditScore.creditReportInfo.score / (creditScore.creditReportInfo.maxScoreValue.toFloat()),
                score = creditScore.creditReportInfo.score.toString()
            )
        }
    }
}
