package com.bryenvieira.clearscoretechtest.userinterface.homescreen

data class CreditScoreUiModel(
    val gapWidth: Float,
    val masterProgress: Float,
    val score: String
)
