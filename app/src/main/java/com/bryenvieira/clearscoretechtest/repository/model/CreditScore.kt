package com.bryenvieira.clearscoretechtest.repository.model

import com.google.gson.annotations.SerializedName

data class CreditScore(
    @SerializedName("creditReportInfo")
    val creditReportInfo: CreditReportInfo
)

data class CreditReportInfo(
    val score: Int,
    val maxScoreValue: Int,
    val minScoreValue: Int
)
