package com.bryenvieira.clearscoretechtest.repository.api

import com.bryenvieira.clearscoretechtest.BuildConfig
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    factory { provideCreditScoreApi(get()) }
    single { provideRetrofit() }
}

fun provideRetrofit(): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(OkHttpClient().newBuilder().build())
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideCreditScoreApi(retrofit: Retrofit): CreditScoreApi = retrofit.create(CreditScoreApi::class.java)
