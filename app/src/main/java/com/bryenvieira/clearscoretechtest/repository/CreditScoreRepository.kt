package com.bryenvieira.clearscoretechtest.repository

import com.bryenvieira.clearscoretechtest.repository.api.CreditScoreApi
import org.koin.dsl.module

val creditScoreModule = module {
    factory { CreditScoreRepository(get()) }
}

class CreditScoreRepository(private val creditScoreApi: CreditScoreApi) {
    suspend fun getCreditScore() = creditScoreApi.getCreditScore()
}
