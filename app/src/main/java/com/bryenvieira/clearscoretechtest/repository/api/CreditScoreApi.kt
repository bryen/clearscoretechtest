package com.bryenvieira.clearscoretechtest.repository.api

import com.bryenvieira.clearscoretechtest.repository.model.CreditScore
import retrofit2.http.GET

interface CreditScoreApi {
    @GET("endpoint.json")
    suspend fun getCreditScore(): CreditScore
}